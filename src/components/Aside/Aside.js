import React from 'react';
import './Aside.css';
import {Link} from "react-router-dom";
// import {Link} from "react-router-dom";

const CATEGORIES = [
  {title: 'Показать все', id: 'all'},
  {title: 'Цитаты великих людей', id: 'famous-people'},
  {title: 'Мотивация на каждый день', id: 'motivation'},
  {title: 'Сборник цитат Кличко', id: 'klichko'},
  {title: 'Юмор про Чака Норриса', id: 'chuck-norris'}
];

const Aside = () => {
  return(
    <div className="categories">
      <ul>
        {CATEGORIES.map(item => {
          return <Link to={"/quotes/" + item.id} key={item.id} className="aside-link"><li>{item.title}</li></Link>
        })}
      </ul>
    </div>
  )
};

export default Aside;