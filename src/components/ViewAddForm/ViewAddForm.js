import React from 'react';
import './ViewAddForm.css';

const ViewAddForm = (props) => {
  return(
    <div className="add-quote-container">
      <h2>Добавление новой цитаты</h2>
      <h4>Категория</h4>
      <select className="add-select" onChange={props.changedSelect}>
        <option>выберите категорию</option>
        <option name="category" value="famous-people">Цитаты великих людей</option>
        <option name="category" value="motivation">Мотивация на каждый день</option>
        <option name="category" value="klichko">Сборник цитат Кличко</option>
        <option name="category" value="chuck-norris">Юмор про Чака Норриса</option>
      </select>
      <h4>Автор</h4>
      <input className="add-input" type="text" name="author" placeholder="Enter Author"
             onChange={props.changedInput}/>
      <h4>Текст цитаты</h4>
      <textarea className="text-area" name="text" id="" cols="50" rows="10" placeholder="Enter text"
                onChange={props.changedText} />
      <input type="submit" value="Сохранить" onClick={props.saved}/>
    </div>
  )
};

export default ViewAddForm;