import React from 'react';
import './Nav.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
  return(
    <nav className='main-nav'>
      <ul>
        <li><NavLink to="/quotes" className='link'>Quotes</NavLink></li>
        <li><NavLink to="/add-quote" className='link'>Add new quote</NavLink></li>
      </ul>
    </nav>
  )
};

export default Nav;