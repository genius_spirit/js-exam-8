import React, {Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Nav from "./components/Nav/Nav";
import Quotes from "./containers/Quotes/Quotes";
import AddQuote from "./containers/AddQuote/AddQuote";
import QuotesList from "./components/QuotesList/QuotesList";
import EditQuote from "./containers/EditQuote/EditQuote";

const App = () => {
  return(
    <Fragment>
      <Nav/>
      <Switch>
        <Route exact path="/" component={Quotes}/>
        <Route exact path="/quotes" component={Quotes}/>
        <Route exact path="/quotes/:id" component={QuotesList}/>
        <Route exact path="/quotes/:id/edit-quote" component={EditQuote}/>
        <Route exact path="/add-quote" component={AddQuote}/>
        <Route render={() => <h1 style={{textAlign: 'center'}}>Page not found</h1>} />
      </Switch>
    </Fragment>
  )
};

export default App;