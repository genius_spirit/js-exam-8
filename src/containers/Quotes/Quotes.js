import React, {Component} from 'react';
import Aside from "../../components/Aside/Aside";
import axios from 'axios';
import './Quotes.css';
import {Link} from "react-router-dom";

class Quotes extends Component {

  state = {
    quotes: []
  };

  getQuotes = () =>{
    axios.get('/quotes.json').then(r => {
      r.data ? this.setState({quotes: r.data}) : this.setState({quotes: []});
    })
  };

  deleteQuoteHandler = (id) => {
    axios.delete(`/quotes/${id}.json`).then(() => {
      this.getQuotes();
    });
  };

  componentDidMount() {
    this.getQuotes();
  }

  render() {
    return(
      <div className="quotes-wrapper">
        <Aside/>
        <div className="quotes-list">
          {Object.keys(this.state.quotes).map(itemId => {
            return (
              <div key={itemId} className="quotesList-item">
                <div className="quote-item">
                  <blockquote key={itemId}>
                    <p>{this.state.quotes[itemId].text}</p>
                    <footer>— <cite>{this.state.quotes[itemId].author}</cite></footer>
                  </blockquote>
                </div>
                <div className="icons-panel">
                  <i className="material-icons icon icon-delete" onClick={() => this.deleteQuoteHandler(itemId)}>delete_sweep</i>
                  <Link to={`/quotes/${itemId}/edit-quote`}><i className="material-icons icon icon-edit">create</i></Link>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

export default Quotes;