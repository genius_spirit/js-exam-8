import React, { Component } from 'react';
import axios from "axios/index";

class EditQuote extends Component {

  state = {
    category: '',
    author: '',
    text: ''
  };

  changeValueHandler = (event) => {
    this.setState({[event.target.name]: event.target.value})
  };

  getQuoteOfCategory = () =>{
    let id = this.props.match.params.id;
    axios.get(`/quotes/${id}.json`).then(r => {
      this.setState({category: r.data.category, author: r.data.author, text: r.data.text});
    })
  };

  updateHandler = () =>{
    let id = this.props.match.params.id;
    axios.patch(`/quotes/${id}.json`, this.state).then(() => {
      this.props.history.push('/quotes');
    })
  };

  componentDidMount() {
    this.getQuoteOfCategory();
  }

  render() {
    return(
      <div className="add-quote-container">
        <h2>Редактирование цитаты</h2>
        <h4>Категория</h4>
        <input className="add-input" type="text" name="category"
               onChange={(event) => this.changeValueHandler(event)} value={this.state.category}/>
        <h4>Автор</h4>
        <input className="add-input" type="text" name="author"
               onChange={(event) => this.changeValueHandler(event)} value={this.state.author}/>
        <h4>Текст цитаты</h4>
        <textarea className="text-area" name="text" id="" cols="50" rows="10"
                  onChange={(event) => this.changeValueHandler(event)} value={this.state.text}/>
        <input type="submit" value="Сохранить" onClick={this.updateHandler}/>
      </div>
    )
  }
}

export default EditQuote;